import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Leandro Augusto de Carvalho
 */
public class Pratica31 {

    private static Date inicio;
    private static String meuNome = "leandro augusto de carvalho";
    private static GregorianCalendar dataNascimento;
    private static Date fim;

    public static void main(String[] args) {
        inicio = new Date();
        System.out.println(meuNome.toUpperCase());
        System.out.println(meuNome.toUpperCase().charAt(19) + meuNome.toLowerCase().substring(20) + ", " + meuNome.toUpperCase().charAt(0) + ". " + meuNome.toUpperCase().charAt(8) + ". " + meuNome.toUpperCase().charAt(16) + ".");
        dataNascimento = new GregorianCalendar(1982, Calendar.FEBRUARY, 20);        
        System.out.println((float)(inicio.getTime() - dataNascimento.getTimeInMillis())/86400000);
        fim = new Date();
        System.out.println((float)(fim.getTime() - inicio.getTime()));
    }
}
